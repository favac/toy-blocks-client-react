import {
  GET_NODE_BLOCKS_START,
  GET_NODE_BLOCKS_SUCCESS,
  GET_NODE_BLOCKS_FAILURE
} from '../constants/actionTypes'

export default function blocksReducer(state, action) {
  let list, nodeIndex
  switch (action.type) {
    case GET_NODE_BLOCKS_START:
      return {
        ...state
      }
    case GET_NODE_BLOCKS_SUCCESS:
      list = state.list
      nodeIndex = state.list.findIndex(p => p.url === action.nodeUrl)
      if (nodeIndex >= 0) {
        list = [
          ...state.list.slice(0, nodeIndex),
          {
            ...state.list[nodeIndex],
            blocks: action.res.data
          },
          ...state.list.slice(nodeIndex + 1)
        ]
      }
      return {
        ...state,
        list
      }
    case GET_NODE_BLOCKS_FAILURE:
      return {
        ...state
      }

    default:
      return state
  }
}
