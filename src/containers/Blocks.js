import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../actions/blocks'
import Block from '../components/Block'

export class Blocks extends React.Component {
  componentDidMount = () => {
    this.props.actions.getNodeBlocks(this.props.nodeUrl)
  }
  render() {
    const { blocks = [] } = this.props
    return (
      <div>
        <h3>Blocks</h3>
        {blocks.map(block => {
          return <Block block={block} key={block.id}></Block>
        })}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { blocks: state.blocks }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

Blocks.propTypes = {
  blocks: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  nodeUrl: PropTypes.string
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Blocks)
