import fetch from 'cross-fetch'
import * as types from '../constants/actionTypes'

const getNodeBlocksStart = nodeUrl => {
  return {
    type: types.GET_NODE_BLOCKS_START,
    nodeUrl
  }
}

const getNodeBlocksFailure = nodeUrl => {
  return {
    type: types.GET_NODE_BLOCKS_FAILURE,
    nodeUrl
  }
}

const getNodeBlocksSuccess = (nodeUrl, res) => {
  return {
    type: types.GET_NODE_BLOCKS_SUCCESS,
    nodeUrl,
    res
  }
}

export function getNodeBlocks(nodeUrl) {
  return async dispatch => {
    try {
      dispatch(getNodeBlocksStart(nodeUrl))
      const res = await fetch(`${nodeUrl}/api/v1/blocks`)
      if (res.status >= 400) {
        dispatch(getNodeBlocksFailure(nodeUrl))
      }
      const json = await res.json()

      dispatch(getNodeBlocksSuccess(nodeUrl, json))
    } catch (error) {
      dispatch(getNodeBlocksFailure(nodeUrl))
    }
  }
}
