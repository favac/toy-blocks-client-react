import React from 'react'

export default function Block({ block }) {
  return (
    <div>
      <span>{block.id}</span> <span>{block.attributes.data}</span>
    </div>
  )
}
